import tensorflow as tf
from keras import backend as K
from keras.models import load_model, Model, Sequential
from keras.optimizers import Adam
from keras.layers import Input, Concatenate, Lambda
from scipy.misc import imread
import numpy as np
from matplotlib import pyplot as plt

from models.keras_ssd7 import build_model
from keras_loss_function.keras_ssd_loss import SSDLoss
from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras_layers.keras_layer_DecodeDetections import DecodeDetections
from keras_layers.keras_layer_DecodeDetectionsFast import DecodeDetectionsFast
from keras_layers.keras_layer_L2Normalization import L2Normalization
from data_generator.object_detection_2d_data_generator import DataGenerator
from eval_utils.average_precision_evaluator import Evaluator
from bainmary.models.seg_gan import SegGan
from bainmary.utils.expe import load_params
import autodebug

# Set a few configuration parameters.
img_height = 128
img_width = 128
n_classes = 1
model_mode = 'inference'


K.clear_session() # Clear previous models from memory.

detector_model = build_model(image_size=(img_height, img_width, 1),
                n_classes=n_classes,
                mode=model_mode,
                l2_regularization=0.0005,
                scales=[0.08, 0.16, 0.32, 0.64, 0.96],
                aspect_ratios_global=[0.5, 1.0, 2.0],
                variances=[1.0, 1.0, 1.0, 1.0],
                normalize_coords=True,
                confidence_thresh=0.01,
                iou_threshold=0.45,
                top_k=200,
                nms_max_output_size=400)

# 2: Load the trained weights into the model.

# TODO: Set the path of the trained weights.
# weights_path = '/home/kyc/prog/ssd_keras/ssd7_epoch-200_loss-0.0093_val_loss-0.0101.h5'
weights_path = '/home/kyc/prog/bainmary/data/results/synthetic_detection/expe-1/run-0/train-0/ssd7_epoch.h5'

detector_model.load_weights(weights_path, by_name=True)

# 3: Compile the model so that Keras won't complain the next time you load it.

adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)

detector_model.compile(optimizer=adam, loss=ssd_loss.compute_loss)

params = load_params('/home/kyc/prog/bainmary/experiments/seg_gan_singleclass/params.json')
GAN = SegGan(params['gan_model'])
GAN.build()
GAN.adversarial_model().load_weights("/home/kyc/prog/bainmary/data/results/seg_gan_singleclass/expe-8/run-4/sub_sample-1987/train-0/weights-50.h5")

# rgb_inputs = Input(shape=(img_height, img_width, 1))
# # gray_inputs = Lambda(lambda x: tf.image.rgb_to_grayscale(x))(rgb_inputs)
# x = GAN.generator_model()(rgb_inputs)
# x = detector_model(x)
# model = Model(rgb_inputs, x)
# model.summary()
model = Sequential()
model.add(GAN.generator_model())
model.add(detector_model)

# Or

# ## 2. Create a data generator for the evaluation dataset
# 
# Instantiate a `DataGenerator` that will serve the evaluation dataset during the prediction phase.

# In[5]:

classes = ['background', 'sharp']
dataset = DataGenerator(load_images_into_memory=True, hdf5_dataset_path=None)
images_dir = '../bainmary/data/omr_dataset/accidental_dataset_ssd_keras2'
labels_filename = '../bainmary/data/omr_dataset/accidental_dataset_ssd_keras2/labels_trainval.csv'
dataset.parse_csv(images_dir=images_dir,
                  labels_filename=labels_filename,
                  input_format=['image_name', 'xmin', 'xmax', 'ymin', 'ymax', 'class_id'],
                  include_classes='all')
val_dataset = DataGenerator(load_images_into_memory=True, hdf5_dataset_path=None)
images_dir = '../bainmary/data/omr_dataset/ShiftedIsolatedSymbol_ssd_keras2'
labels_filename = '../bainmary/data/omr_dataset/ShiftedIsolatedSymbol_ssd_keras2/labels_val.csv'
val_dataset.parse_csv(images_dir=images_dir,
                  labels_filename=labels_filename,
                  input_format=['image_name', 'xmin', 'xmax', 'ymin', 'ymax', 'class_id'],
                  include_classes='all')


evaluator = Evaluator(model=model,
                      n_classes=n_classes,
                      data_generator=dataset,
                      model_mode=model_mode)

for i in range(1):
    iou_threshold = (i * 0.25) / 10 + 0.5
    results = evaluator(img_height=img_height,
                        img_width=img_width,
                        batch_size=8,
                        data_generator_mode='resize',
                        round_confidences=False,
                        matching_iou_threshold=iou_threshold,
                        border_pixels='include',
                        sorting_algorithm='quicksort',
                        average_precision_mode='integrate',
                        num_recall_points=11,
                        ignore_neutral_boxes=True,
                        return_precisions=True,
                        return_recalls=True,
                        return_average_precisions=True,
                        verbose=True)

    __import__('pdb').set_trace()
    mean_average_precision, average_precisions, precisions, recalls = results


# ## 4. Visualize the results
# 
# Let's take a look:

# In[10]:


    for i in range(1, len(average_precisions)):
        print("{:<14}{:<6}{}".format(classes[i], 'AP', round(average_precisions[i], 3)))
    print()
    print("{:<14}{:<6}{}".format('','mAP', round(mean_average_precision, 3)))


# In[11]:


    m = max((n_classes + 1) // 2, 2)
    n = 2

    fig, cells = plt.subplots(m, n, figsize=(n*8,m*8))
    for i in range(m):
        for j in range(n):
            if n*i+j+1 > n_classes: break
            cells[i, j].plot(recalls[n*i+j+1], precisions[n*i+j+1], color='blue', linewidth=1.0)
            cells[i, j].set_xlabel('recall', fontsize=14)
            cells[i, j].set_ylabel('precision', fontsize=14)
            cells[i, j].grid(True)
            cells[i, j].set_xticks(np.linspace(0,1,11))
            cells[i, j].set_yticks(np.linspace(0,1,11))
            cells[i, j].set_title("{}, AP: {:.3f}".format(classes[n*i+j+1], average_precisions[n*i+j+1]), fontsize=16)
    # plt.show()

    evaluator.get_num_gt_per_class(ignore_neutral_boxes=True,
                                   verbose=False,
                                   ret=False)

    evaluator.match_predictions(ignore_neutral_boxes=True,
                                matching_iou_threshold=iou_threshold,
                                border_pixels='include',
                                sorting_algorithm='quicksort',
                                verbose=True,
                                ret=False)

    precisions, recalls = evaluator.compute_precision_recall(verbose=True, ret=True)

    average_precisions = evaluator.compute_average_precisions(mode='integrate',
                                                              num_recall_points=11,
                                                              verbose=True,
                                                              ret=True)

    mean_average_precision = evaluator.compute_mean_average_precision(ret=True)


    for i in range(1, len(average_precisions)):
        print("{:<14}{:<6}{}".format(classes[i], 'AP', round(average_precisions[i], 3)))
    print()
    print("{:<14}{:<6}{}".format('','mAP', round(mean_average_precision, 3)))

